# FitCatch Project

TrustVox project of workout log app from the FrontEnd Developer Test

# Description

Simple workout log for Running, Swimming and Cycling sports. Project developed in AngularJS framework with Angular-Material components


Link:
[Workout Log Online](https://fitcatch.herokuapp.com/)


