


angular
	.module('fitCatch', ['ngMaterial'])


	.controller('AppCtrl', function($scope, $mdDialog, $filter) {
		$scope.imagePath = 'assets/img/make-circle-shoe.png';

		$scope.test = '';

		$scope.count_hours = 0;
		$scope.workouts = [];
		$scope.kindModel = "";
		$scope.orders = [
			{
		      name : 'A-Z',
		      icon : 'assets/icon/ic_arrow_drop_up_black_24px.svg',
		      id : 'a_z_up',
		      expression : 'title'
		    },
			{
		      name : 'A-Z',
		      icon : 'assets/icon/ic_arrow_drop_down_black_24px.svg',
		      id : 'a_z_down',
		      expression : '-title'
		    },
			{
		      name : 'Time Spend',
		      icon : 'assets/icon/ic_arrow_drop_up_black_24px.svg',
		      id : 'time_up',
		      expression : 'time'
		    },
			{
		      name : 'Time Spend',
		      icon : 'assets/icon/ic_arrow_drop_down_black_24px.svg',
		      id : 'time_down',
		      expression : '-time'
		    },
			{
		      name : 'Date',
		      icon : 'assets/icon/ic_arrow_drop_up_black_24px.svg',
		      id : 'date_up',
		      expression : 'date'
		    },
			{
		      name : 'Date',
		      icon : 'assets/icon/ic_arrow_drop_down_black_24px.svg',
		      id : 'date_down',
		      expression : '-date'

		    }
		];
		$scope.kinds = [
			{
				title : "Run",
				img   : "assets/img/running.svg",
				value : "running",
				hours_count : 0,
				color : "#eb7053"
			},
			{
				title : "Swimming",
				img   : "assets/img/swimming.svg",
				value : "swimming",
				hours_count : 0,
				color : "#afebff"
			},
			{
				title : "Bike",
				img   : "assets/img/cycling.svg",
				value : "cycling",
				hours_count : 0,
				color : "#88928f"
			}
		];

		$scope.showAddWorkoutDialog = function(ev, kinds) {
		    $mdDialog.show({
		      controller: DialogController,
		      templateUrl: 'html/add_workout_dialog.tmpl.html',
		      parent: angular.element(document.body),
		      targetEvent: ev,
		      clickOutsideToClose:true,
		      locals: {dataKinds: kinds, kindModel: ""}
		    })
	        .then(function(newWorkout) {
		    	$scope.workouts.push(newWorkout);
		    	$scope.count_hours += newWorkout.add_hour
	        }, function() {
	        });
		  };

      

        $scope.orderByWorkouts = function(expression) {
        	$scope.workouts = $filter('orderBy')($scope.workouts, expression)
        };


        $scope.deleteWorkout = function(workout) {
        	$scope.count_hours -= $scope.workouts[$scope.workouts.indexOf(workout)].add_hour;
            $scope.workouts.splice($scope.workouts.indexOf(workout), 1);
        };

        $scope.hasWorkouts = function(workouts) {
        	if(workouts.length > 0){
        		return true;
        	}
        	return false;
        }


        function DialogController($scope, $mdDialog, dataKinds, kindModel) {
        	$scope.dataKinds = dataKinds;  
        	$scope.kindModel = "running";
        	$scope.date = new Date();
        	$scope.time = 1;
		    $scope.hide = function() {
		      $mdDialog.hide();
		    };
		    $scope.cancel = function() {
		      $mdDialog.cancel();
		    };
		    $scope.add = function() {
		    	kind = $filter('filter')($scope.dataKinds, {'value': $scope.kindModel}) 
			    newWorkout = {
	        		title : kind[0].title,
	        		time  : $scope.time + "H",
	        		date  : $scope.date.getDate() + "/" + (parseInt($scope.date.getMonth())+1)	 +  "/" +  $scope.date.getFullYear(),
	        		type  : $scope.kindModel,
	        		add_hour : $scope.time,
	        		color : {'background-color': kind[0].color }
	        	}
		      $mdDialog.hide(newWorkout);
		    };
			}

	});
